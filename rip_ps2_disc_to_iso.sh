#!/bin/bash

# Static constants
PS2_GAMES_DIR="${HOME}/Games/retro/sony/ps2/games"

# Input variables
IMG_OUTPUT_NAME=${1}
MULTI_DISK_NUM=${2}

if [ -z "${IMG_OUTPUT_NAME}" ]; then
  echo
  echo "[ERROR]: Output file name required!"
  echo "[FORMAT]: ./rip_ps2_disc_to_iso.sh <output_name_with_no_extension> <[disk_number_if_multi_disk_game]>"

  exit 1
fi

# Local variables
OUTPUT_DIR_NAME="${IMG_OUTPUT_NAME}"

if [ ! -d "${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME}" ]; then
  echo
  echo "[INFO]: Directory ${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME} does not exist. Creating..."

  mkdir -p "${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME}"
  RETURN_CODE=${?}

  if [ ${RETURN_CODE} -ne 0 ]; then
    echo
    echo "[ERROR]: Something went wrong with creation of ${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME}. Exiting..."

    exit ${RETURN_CODE}
  else
    echo
    echo "[SUCCESS]: Directory ${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME} created!"
  fi
fi

if [ -n "${MULTI_DISK_NUM}" ]; then
  IMG_OUTPUT_NAME="${IMG_OUTPUT_NAME}.disk_${MULTI_DISK_NUM}"
fi

dd status="progress" \
  if="/dev/cdrom"    \
  of="${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME}/${IMG_OUTPUT_NAME}.iso"
RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]; then
  echo
  echo "[ERROR]: Something went wrong in ${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME}/${IMG_OUTPUT_NAME}.iso creation. Exiting..."

  exit ${RETURN_CODE}
else
  echo
  echo "[SUCCESS]: ${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME}/${IMG_OUTPUT_NAME}.iso created!"
  echo
  echo "MD5 sum of ${IMG_OUTPUT_NAME}.iso: $(md5sum ${PS2_GAMES_DIR}/${OUTPUT_DIR_NAME}/${IMG_OUTPUT_NAME}.iso)"

  exit ${RETURN_CODE}
fi
