# rip_ps2_disc_to_image

This project houses a simple Bash shellscript that rips the contents of a PlayStation 2 game-disc to a .iso image file, for use with a PlayStation 2 emulator such as PCSX2.

**NOTE**: There is only one dependency that is required for this script to work:

    dd

If you're using Debian GNU/Linux, the necessary packages should already be installed as part of the default installation.

**FINAL NOTE**: This project is not intended to be used for videogame piracy. I neither condone nor practice piracy in any way, shape, or form. Rather, I exercise my rights in accordance with US Copyright Law, specifically section 17 subsection 117. For more information, see the following links about US Copyright Law: Section 17 Subsection 117:

https://www.copyright.gov/help/faq/faq-digital.html

https://www.law.cornell.edu/uscode/text/17/117
